export function allPokemons(state, action) {
  switch (action.type) {
    case 'loaded':
      return {
        ...state,
        isLoading: false,
        pokemons: action.payload,
        list: action.payload.slice(
          state.offset,
          state.offset + state.pageLimit
        ),
        totalPages: Math.ceil(action.payload.length / state.pageLimit)
      };

    case 'changePage': {
      return {
        ...state,
        currentPage: action.payload,
        list: state.pokemons.slice(
          (action.payload - 1) * state.pageLimit,
          (action.payload - 1) * state.pageLimit + state.pageLimit
        )
      };
    }
    default:
      return state;
  }
}
