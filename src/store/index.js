export default async function getPockemons() {
  const response = await fetch('https://graphql-pokemon.now.sh/', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      query:
        '{pokemons(first: 50) {id number name weight { minimum maximum } height { minimum maximum } types attacks { special { name type damage } } image attacks { special { name type damage } } } }'
    })
  });
  const data = await response.json();
  const { pokemons } = data.data;
  return pokemons;
}
