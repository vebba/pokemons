import { useEffect } from 'react';
export const useAPI = (url, dispatch) => {
  useEffect(() => {
    fetch(url, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        query:
          '{pokemons(first: 50) {id number name weight { minimum maximum } height { minimum maximum } types attacks { special { name type damage } } image attacks { special { name type damage } } } }'
      })
    })
      .then(response => response.json())
      .then(result => {
        dispatch({ type: 'loaded', payload: result.data.pokemons });
      });
  }, [dispatch, url]);
};
