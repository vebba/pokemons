import React from 'react';
import ReactDOM from 'react-dom';
import '@babel/polyfill';
import 'whatwg-fetch';

import App from './app';
ReactDOM.render(<App />, document.getElementById('app'));
