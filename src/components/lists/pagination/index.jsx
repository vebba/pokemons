import React from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import tw from 'tailwind.macro';
const PageList = styled.div`
  ${tw`flex items-center pb-4`}
`;
const PageItem = styled.div`
  ${tw`p-2 cursor-pointer text-sm text-grey-dark`}
  color: ${props =>
    props.id === props.currentPage - 1 ? 'white' : 'text-grey-dark'};
  user-select:none;
`;

const PageArrow = styled.div`
  ${tw`p-2 pr-3 pl-3 bg-grey-light cursor-pointer border-grey border-solid border rounded text-grey-darkest font-bold`}
`;

const Pagination = ({ totalPages, dispatch, currentPage }) => {
  return (
    <PageList
      onClick={e => {
        if (e.target.dataset.page === 'next' && currentPage < totalPages) {
          currentPage++;
          dispatch({ type: 'changePage', payload: currentPage });
        }
        if (e.target.dataset.page === 'prev' && currentPage > 1) {
          currentPage--;
          dispatch({ type: 'changePage', payload: currentPage });
        }
        if (e.target.dataset.page === 'num') {
          dispatch({ type: 'changePage', payload: Number(e.target.innerHTML) });
        }
      }}
    >
      <PageArrow data-page={'prev'}>{'<'}</PageArrow>
      {[...Array(totalPages)].map((item, index) => (
        <PageItem
          data-page="num"
          currentPage={currentPage}
          id={index}
          key={`${item}${index}`}
        >
          {index + 1}
        </PageItem>
      ))}
      <PageArrow data-page="next">{'>'}</PageArrow>
    </PageList>
  );
};

Pagination.propTypes = {
  dispatch: PropTypes.func,
  totalPages: PropTypes.number,
  currentPage: PropTypes.number
};

export default Pagination;
