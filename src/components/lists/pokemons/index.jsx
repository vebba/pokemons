import React from 'react';
import PropTypes from 'prop-types';
import { PokemonCard } from '../../cards';
import styled from '@emotion/styled';
import tw from 'tailwind.macro';
const ListWrapper = styled.div`
  ${tw`w-full md:w-3/5`}
`;

const PokemonList = ({ pokemons }) => {
  return (
    <ListWrapper>
      {pokemons.map(pokemon => (
        <PokemonCard key={pokemon.id} pokemon={pokemon} />
      ))}
    </ListWrapper>
  );
};

PokemonList.propTypes = {
  pokemons: PropTypes.array
};

PokemonList.defaultProps = {
  pokemons: []
};
export default PokemonList;
