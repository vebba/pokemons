export { default as PokemonList } from './pokemons/';

export { default as Pagination } from './pagination/';
