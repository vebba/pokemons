import React from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import tw from 'tailwind.macro';

const Wrapper = styled.div`
  ${tw`font-sans  text-grey-darker font-light flex  w-full bg-white mb-3 rounded justify-around items-center`}
`;
const BigTitle = styled.h1`
  ${tw`text-xl font-normal`}
`;
const MediumTitle = styled.h2`
  ${tw`text-base mt-2 font-normal text-sm `}
`;
const Units = styled.p`
  ${tw`text-xs tracking-wide`}
`;
const PokemonImage = styled.div`
  ${tw` p-2  h-32 w-32 bg-contain bg-no-repeat rounded m-2 bg-center`}
  background-image: url(${props => props.url})
`;
const PokemonDetails = styled.div`
  ${tw`flex flex-col p-8`}
`;
const PokemonSkills = styled.div`
  ${tw`flex flex-col p-8 text-xs self-start`}
`;

const PokemonCard = ({ pokemon }) => {
  return (
    <Wrapper>
      <PokemonImage url={pokemon.image} />
      <PokemonDetails>
        <BigTitle>{pokemon.name}</BigTitle>
        <span>{pokemon.types.map(type => `${type} `)}</span>
        <MediumTitle>Weight:</MediumTitle>
        <Units>min: {pokemon.weight.minimum}</Units>
        <Units>max: {pokemon.weight.maximum}</Units>
        <MediumTitle>Height:</MediumTitle>
        <Units>min: {pokemon.height.minimum}</Units>
        <Units>max: {pokemon.height.maximum}</Units>
      </PokemonDetails>
      <PokemonSkills>
        <BigTitle>Special Attacks</BigTitle>
        {pokemon.attacks.special.map((special, index) => (
          <div key={`${index}`}>
            {special.name} : {special.damage}
          </div>
        ))}
      </PokemonSkills>
    </Wrapper>
  );
};

PokemonCard.propTypes = {
  pokemon: PropTypes.object
};

export default PokemonCard;
