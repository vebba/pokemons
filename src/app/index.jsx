import React, { useReducer } from 'react';
import { hot } from 'react-hot-loader/root';
import { PokemonList, Pagination } from '../components/lists';
import styled from '@emotion/styled';
import tw from 'tailwind.macro';
import Loader from '../components/loader';
import { useAPI } from '../hooks/useAPI';
import { allPokemons } from '../reducers/pokemons';

const MainWrapper = styled.main`
  ${tw`font-sans flex w-full h-full flex-col items-center bg-grey-darkest`}
`;

const initialState = {
  pokemons: [],
  isLoading: true,
  currentPage: 1,
  offset: 0,
  pageLimit: 5,
  list: [],
  totalPages: 0
};

function App() {
  const [appState, dispatch] = useReducer(allPokemons, initialState);
  useAPI('https://graphql-pokemon.now.sh/', dispatch);

  return (
    <MainWrapper>
      {appState.isLoading ? (
        <Loader />
      ) : (
        <>
          <PokemonList pokemons={appState.list} />
          <Pagination
            dispatch={dispatch}
            totalPages={appState.totalPages}
            currentPage={appState.currentPage}
          />
        </>
      )}
    </MainWrapper>
  );
}

export default hot(App);
